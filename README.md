# WIP


## Vars 

* OPSI_USER
* OPSI_PASSWORD
* OPSI_DB_NAME
* OPSI_DB_OPSI_USER
* OPSI_DB_OPSI_PASSWORD
* OPSI_DB_ROOT_PASSWORD

## build

If you want to build your own local docker image : 

```bash
 docker build . -t opsi-custom
```
## run

### step 1 : Mysql

```bash
 docker run -itd --name opsi-database -e MYSQL_DATABASE=opsi-db -e MYSQL_USER=opsi-db-user -e MYSQL_PASSWORD=HiOpsI -e MYSQL_ROOT_PASSWORD=Sùperp@ssw0rd -d mysql:5.7
```

### Step 2 : OPSI Link Mysql

```bash
 docker run -h opsi.$domain -it  --link opsi-database:db -e OPSI_DB_NAME=opsi-db -e OPSI_DB_OPSI_USER=opsi-db-user -e OPSI_DB_OPSI_PASSWORD=HiOpsI -e OPSI_USER=sysadmin -e OPSI_PASSWORD=linux123 -P opsi-custom 
```

Note : Take care about the -h arg because if opsiconfd doesn't see a valid fqdn it will crash


# TODO 

[ ] Licence file handle

[ ] Docker compose file

[ ] Kubernetes support

[ ] External Docker TFTP

[ ] Backend MYSQL

[ ] LDAP Authentication

[ ] External Docker syslog

[ ] Documentation