FROM debian:stretch

LABEL maintainer="Antoine GUEVARA <me@antoine-guevara.fr>"

ENV DEBIAN_FRONTEND noninteractive
ENV OPSI_USER $OPSI_USER
ENV OPSI_PASSWORD $OPSI_PASSWORD
ENV OPSI_DB_NAME $OPSI_DB_NAME
ENV OPSI_DB_OPSI_USER $OPSI_DB_OPSI_USER
ENV OPSI_DB_OPSI_PASSWORD $OPSI_DB_OPSI_PASSWORD
ENV OPSI_DB_ROOT_PASSWORD $OPSI_DB_ROOT_PASSWORD

RUN apt-get update -qq && apt-get install -y \
    hostname \
    apt-utils \
    iputils-ping \
    openssl \
    net-tools \
    openssh-client \
    vim \
    wget \
    lsof \
    host \
    python-mechanize \
    p7zip-full \
    cabextract \
    openbsd-inetd \
    pigz \
    cpio \
    samba \
    samba-common \
    smbclient \
    cifs-utils \
    curl \
    mariadb-server

RUN echo "deb http://download.opensuse.org/repositories/home:/uibmz:/opsi:/4.1:/stable/Debian_9.0/ /" > /etc/apt/sources.list.d/opsi.list

RUN wget -O - https://download.opensuse.org/repositories/home:uibmz:opsi:4.1:stable/Debian_9.0/Release.key | apt-key add -


RUN apt-get update -qq && DEBIAN_FRONTEND="noninteractive" apt install -y opsi-tftpd-hpa opsi-server opsi-configed opsi-windows-support

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./scripts/entrypoint.sh /usr/local/bin/

VOLUME ["/var/lib/opsi/", "/etc/opsi/"]


EXPOSE 139/tcp 445/tcp 4447/tcp 69/udp 137/udp 138/udp 69/udp