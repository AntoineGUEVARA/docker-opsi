#!/bin/bash

/usr/sbin/useradd -m -s /bin/bash $OPSI_USER

echo "$OPSI_USER:$OPSI_PASSWORD" | chpasswd

echo -e "$OPSI_PASSWORD\n$OPSI_PASSWORD\n" | smbpasswd -s -a $OPSI_USER

/usr/sbin/usermod -aG opsiadmin $OPSI_USER

/usr/sbin/usermod -aG pcpatch $OPSI_USER

cat > /etc/opsi/backends/mysql.conf <<-END 
# -*- coding: utf-8 -*-

module = 'MySQL'
config = {
    "address":                   u"db",
    "database":                  u"${OPSI_DB_NAME}",
    "username":                  u"${OPSI_DB_OPSI_USER}",
    "password":                  u"${OPSI_DB_OPSI_PASSWORD}",
    "databaseCharset":           "utf8",
    "connectionPoolSize":        20,
    "connectionPoolMaxOverflow": 10,
    "connectionPoolTimeout":     30
}

END

/usr/bin/opsi-setup --init-current-config

/usr/bin/opsi-setup --set-rights


/usr/bin/opsiconfd -l 5